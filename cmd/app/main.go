package main

import (
	"gitlab.com/magaserdarov/querymonitoring/internal"
	"gitlab.com/magaserdarov/querymonitoring/internal/config"
	"gitlab.com/magaserdarov/querymonitoring/internal/delivery/rest"
	"gitlab.com/magaserdarov/querymonitoring/internal/repository/postgres"
	"gitlab.com/magaserdarov/querymonitoring/internal/server"
	"gitlab.com/magaserdarov/querymonitoring/internal/usecase"

	"go.uber.org/fx"
)

func main() {
	app := fx.New(
		fx.Provide(
			config.New,
			server.New,
			fx.Annotate(
				rest.NewHandler,
				fx.As(new(rest.ServerInterface)),
			),
			fx.Annotate(
				usecase.NewQueriesServiceImpl,
				fx.As(new(internal.QueryService)),
			),
			fx.Annotate(
				postgres.NewQueriesRepo,
				fx.As(new(internal.QueryStorage)),
			),
			fx.Annotate(
				postgres.NewPostgresClientProvider,
				fx.As(new(postgres.DBTX)),
			),
			fx.Annotate(
				postgres.New,
				fx.As(new(postgres.QueriesInterface)),
			),
			rest.NewCache,
		),
		fx.Invoke(server.RegisterRoutes),
	)

	app.Run()
}
