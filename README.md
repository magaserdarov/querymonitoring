# Slow Query Monitoring API for PostgreSQL

## Description
PostgreSQL allows you to keep track of queries with certain performance metrics and statistics, which comes in handy when identifying slow queries.

Under the hood, the service uses the `pg_stat_statements` [extension](https://www.postgresql.org/docs/current/pgstatstatements.html), a module that provides a means for tracking the planning and execution statistics of all SQL statements executed by your PostgreSQL server, to provide you with the basic information that can be useful for identifying slow queries.

## Query statistics
These are the entries provided by service which are deduced via the `pg_stat_statements`:

| Field         | Description                       |
|---------------|-------------------------------------------------------------|
| Calls         | Number of times the statement was executed                  |
| MaxExecTime   | Maximum time spent executing the statement, in milliseconds |
| MinExecTime   | Minimum time spent executing the statement, in milliseconds |
| MeanExecTime  | Mean time spent executing the statement, in milliseconds    |
| TotalExecTime | Total time spent executing the statement, in milliseconds   |
| Dbid          | OID of database in which the statement was executed         |
| Userid        | OID of user who executed the statement                      |


## Pre-requisites
To use this service, you’ll need to create the `pg_stat_statements` extension that can be done via the following `CREATE EXTENSION` command:

```shell
CREATE EXTENSION pg_stat_statements;
```

> NOTE: PostgreSQL server restart is needed to add or remove the module

## Setup

### Download source code from [repository](https://gitlab.com/magaserdarov/querymonitoring.git):
```shell
git clone https://gitlab.com/magaserdarov/querymonitoring.git && \
cd querymonitoring
```

### Set environment variables
Look `.env.sample` file in root directory, change values and run:

```shell
source .env.sample
```

| Variable      | Description                                                 |
|-------------------------------|---------------------------------------------|
| HYGH_DB_DSN                   | Database connection string                  |
| HYGH_APP_QUERY_EXEC_THRESHOLD | This variable defines what is the `slowquery` for your system (default: 20ms) |



### Install dependencies 
```shell
go mod tidy
```

## Migrate
For demo purposes service provides CRUD of dummy data. Use `make migrate` to apply migrations. This will create `dummy` table in database.
```shell
make migrate
```
> NOTE: For demo purposes `SELECT`, `UPDATE`, `DELETE` queries use `pg_sleep()` PostgreSQL function with random amount of time to simulate slow query.

## Start the server
```shell
make run
```

## API specification

Check [API specification](https://gitlab.com/magaserdarov/querymonitoring/-/blob/main/api/openapi/app.yml?ref_type=heads) in `api/openapi/app.yml` or run mock api server using [prism](https://stoplight.io/open-source/prism) by runnig:
```shell
make mockapi
```

## Test
Run tests:
```shell
go test ./...
```






