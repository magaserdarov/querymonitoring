# docker run -d --name hygh_db -p 5432:5432 -e POSTGRES_USER=dbuser -e POSTGRES_DB=mydb -e POSTGRES_PASSWORD=dbsecret -v "$PWD/my-postgres.conf":/etc/postgresql/postgresql.conf   postgres -c 'config_file=/etc/postgresql/postgresql.conf'


openapi-install:
	go install github.com/deepmap/oapi-codegen/v2/cmd/oapi-codegen@latest

.PHONY: openapi
openapi: openapi-install
	${GOBIN}/oapi-codegen -generate types -o internal/delivery/rest/types.openapi.gen.go -package rest api/openapi/app.yml
	${GOBIN}/oapi-codegen -generate fiber -o internal/delivery/rest/api.openapi.gen.go -package rest api/openapi/app.yml

sql-gen:
	go install github.com/sqlc-dev/sqlc/cmd/sqlc@latest
	cd internal/repository/postgres && sqlc generate

gen:
	go generate ./...

migrate:
	migrate -database=${HYGH_DB_DSN} -path migrations up

setup: openapi sql-gen gen

run:
	go run cmd/app/main.go

mockapi:
	$(shell which prism) mock -d api/openapi/app.yml