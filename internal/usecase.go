package internal

import (
	"context"

	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

//go:generate mockgen -source ./usecase.go -package mocks -destination ./usecase/mocks/queries.mock.gen.go
type QueryService interface {
	GetQueries(ctx context.Context, params models.GetQueriesParams) (*models.QueryList, error)

	GetDummy(ctx context.Context) ([]models.Dummy, error)
	DeleteDummyId(ctx context.Context, id int32) error
	PutDummyId(ctx context.Context, req *models.Dummy) error
	AddDummyData(ctx context.Context, name string) (models.Dummy, error)
}
