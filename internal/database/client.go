package database

import (
	"database/sql"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/magaserdarov/querymonitoring/internal/config"
)

func NewPostgresConnection(cfg *config.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", cfg.DB.DSN)
	if err != nil {
		return nil, errors.Wrap(err, "sql.Connect")
	}
	// check db connection
	if err := db.Ping(); err != nil {
		return nil, errors.Wrap(err, "sql.Connect failed")
	}

	return db, nil
}
