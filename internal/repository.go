package internal

import (
	"context"

	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

//go:generate mockgen -source ./repository.go -package mocks -destination ./repository/postgres/mocks/repository.mock.gen.go

type QueryStorage interface {
	GetSlowQueries(ctx context.Context, params models.QueryFilters) (*models.QueryList, error)
	AddDummyData(ctx context.Context, name string) (models.Dummy, error)

	GetDummy(ctx context.Context) ([]models.Dummy, error)
	DeleteDummyId(ctx context.Context, id int32) error
	PutDummyId(ctx context.Context, req *models.Dummy) error
}
