package rest

import (
	"net/http"

	faker "github.com/go-faker/faker/v4"
	"github.com/go-faker/faker/v4/pkg/options"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/magaserdarov/querymonitoring/internal"
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

type Handler struct {
	queryManager internal.QueryService
}

func NewHandler(queryManager internal.QueryService) *Handler {
	return &Handler{
		queryManager: queryManager,
	}
}

func (h *Handler) GetQueries(ctx *fiber.Ctx, params GetQueriesParams) error {
	c := ctx.Context()

	p := models.GetQueriesParams{
		Page:           params.Page,
		PerPage:        params.PerPage,
		Statement:      (*string)(params.Statement),
		SortByDuration: (*string)(params.SortByDuration),
	}
	list, err := h.queryManager.GetQueries(c, p)
	if err != nil {
		return err
	}

	res := QueryList{}
	queries := make([]Query, len(list.Queries))

	for i := range list.Queries {
		queries[i] = Query{
			Dbid:          list.Queries[i].Dbid,
			Userid:        list.Queries[i].Userid,
			Calls:         &list.Queries[i].Calls,
			MaxExecTime:   &list.Queries[i].MaxExecTime,
			MeanExecTime:  &list.Queries[i].MeanExecTime,
			MinExecTime:   &list.Queries[i].MinExecTime,
			TotalExecTime: &list.Queries[i].TotalExecTime,
			Query:         list.Queries[i].Query,
		}
	}

	res.Queries = queries
	res.Meta = Pagination{
		Page:       list.Meta.Page,
		PerPage:    list.Meta.PerPage,
		TotalPages: list.Meta.TotalPages,
	}

	return ctx.Status(http.StatusOK).JSON(res)
}

func (h *Handler) GetDummy(c *fiber.Ctx) error {
	resp, err := h.queryManager.GetDummy(c.Context())
	if err != nil {
		return err
	}
	res := make([]Dummy, len(resp))
	for i := 0; i < len(resp); i++ {
		res[i] = Dummy{
			Id:   int64(resp[i].Id),
			Name: resp[i].Name,
		}
	}
	return c.Status(http.StatusOK).JSON(res)
}

func (h *Handler) PostDummy(c *fiber.Ctx) error {

	d := new(models.DummyRequest)

	if err := c.BodyParser(d); err != nil {
		return err
	}
	resp, err := h.queryManager.AddDummyData(c.Context(), d.Name)
	if err != nil {
		return err
	}
	body := Dummy{
		Name: resp.Name,
		Id:   int64(resp.Id),
	}
	return c.Status(http.StatusOK).JSON(body)
}

func (h *Handler) DeleteDummyId(c *fiber.Ctx, id int32) error {
	return h.queryManager.DeleteDummyId(c.Context(), id)
}

func (h *Handler) PutDummyId(c *fiber.Ctx, id int32) error {
	var dummy models.Dummy
	if err := c.BodyParser(&dummy); err != nil {
		return err
	}
	dummy.Id = id
	err := h.queryManager.PutDummyId(c.Context(), &dummy)
	if err != nil {
		return err
	}
	res := Dummy{
		Id:   int64(dummy.Id),
		Name: dummy.Name,
	}
	return c.Status(http.StatusOK).JSON(res)
}

func (h *Handler) PostDummyGenerateAmount(c *fiber.Ctx, amount GenerateAmountParam) error {
	if amount <= 0 {
		return c.Status(http.StatusBadRequest).JSON(Error{Code: 400, Message: "amount must be positive"})
	}

	d := make([]Dummy, amount)
	if err := faker.FakeData(&d,
		options.WithRandomMapAndSliceMaxSize(amount),
		options.WithRandomMapAndSliceMinSize(amount),
		options.WithFieldsToIgnore("ID"),
	); err != nil {
		return c.Status(http.StatusInternalServerError).JSON(Error{Code: 500, Message: "failed to generate dummy data"})
	}

	ctx := c.Context()
	for _, row := range d {
		if _, err := h.queryManager.AddDummyData(ctx, row.Name); err != nil {
			println("AddDummyData error:", err.Error())
			continue
		}
	}

	return c.Status(http.StatusOK).JSON(Generated{Message: "generated"})
}
