package rest

import (
	"github.com/dgraph-io/ristretto"
)

func NewCache() (*ristretto.Cache, error) {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,     // number of counters
		MaxCost:     1 << 30, // maximum cost of cache
		BufferItems: 64,      // number of keys per Get buffer
	})
	if err != nil {
		return nil, err
	}
	return cache, nil
}
