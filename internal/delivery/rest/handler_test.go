package rest

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
	usecaseMock "gitlab.com/magaserdarov/querymonitoring/internal/usecase/mocks"
)

func TestGetQueries(t *testing.T) {
	testCases := []struct {
		name          string
		params        string
		buildStubs    func(uc *usecaseMock.MockQueryService)
		checkResponse func(t *testing.T, recorder *http.Response)
	}{
		{
			name:   "OK",
			params: "?page=1&per_page=1",
			buildStubs: func(uc *usecaseMock.MockQueryService) {
				var (
					page     int32 = 1
					per_page int32 = 1
				)
				params := models.GetQueriesParams{
					Page:    &page,
					PerPage: &per_page,
				}
				sql := "select count(*) from pg_stat_statements where query ilike $1"
				dbid := 16384
				userid := 10
				res := &models.QueryList{
					Queries: []models.Query{
						{
							Calls:         4,
							Dbid:          &dbid,
							MaxExecTime:   9.362917,
							MeanExecTime:  3.676177,
							MinExecTime:   1.324249,
							Query:         &sql,
							TotalExecTime: 14.704708,
							Userid:        &userid,
						},
					},
					Meta: models.Pagination{
						Page:       1,
						PerPage:    1,
						TotalPages: 1,
					},
				}
				uc.EXPECT().GetQueries(gomock.Any(), params).Times(1).Return(res, nil)

			},
			checkResponse: func(t *testing.T, recorder *http.Response) {
				require.Equal(t, http.StatusOK, recorder.StatusCode)
			},
		},
		{
			name:   "invalid per page param",
			params: "?per_page=invalid",
			buildStubs: func(uc *usecaseMock.MockQueryService) {
				uc.EXPECT().GetQueries(gomock.Any(), gomock.Any()).Times(0)
			},
			checkResponse: func(t *testing.T, recorder *http.Response) {
				require.Equal(t, http.StatusBadRequest, recorder.StatusCode)
			},
		},
		{
			name:   "invalid  page param",
			params: "?page=invalid",
			buildStubs: func(uc *usecaseMock.MockQueryService) {
				uc.EXPECT().GetQueries(gomock.Any(), gomock.Any()).Times(0)
			},
			checkResponse: func(t *testing.T, recorder *http.Response) {
				require.Equal(t, http.StatusBadRequest, recorder.StatusCode)
			},
		},
		{
			name: "InternalError",
			buildStubs: func(uc *usecaseMock.MockQueryService) {
				uc.EXPECT().
					GetQueries(gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil, fmt.Errorf("eror happened"))
			},
			checkResponse: func(t *testing.T, recorder *http.Response) {
				require.Equal(t, http.StatusInternalServerError, recorder.StatusCode)
			},
		},
	}
	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			uc := usecaseMock.NewMockQueryService(ctrl)
			tc.buildStubs(uc)

			e := fiber.New()
			api := e.Group("/api")
			grp := api.Group("/v1")

			handler := NewHandler(uc)
			RegisterHandlers(grp, handler)
			url := fmt.Sprintf("/api/v1/queries%s", tc.params)
			req := httptest.NewRequest(http.MethodGet, url, nil)

			rec, err := e.Test(req)
			require.NoError(t, err)
			tc.checkResponse(t, rec)

		})
	}
}

// func requireBodyMatchAccount(t *testing.T, body *bytes.Buffer, account Account) {
// 	data, err := ioutil.ReadAll(body)
// 	require.NoError(t, err)

// 	var gotAccount Account
// 	err = json.Unmarshal(data, &gotAccount)
// 	require.NoError(t, err)
// 	require.Equal(t, account.Id, gotAccount.Id)
// 	require.Equal(t, account.Currency, gotAccount.Currency)
// 	require.Equal(t, account.Balance, gotAccount.Balance)
// }

// func requireBodyMatchBalance(t *testing.T, body *bytes.Buffer, balance BalanceResponse) {
// 	data, err := io.ReadAll(body)
// 	require.NoError(t, err)

// 	var gotBalance BalanceResponse
// 	err = json.Unmarshal(data, &gotBalance)
// 	require.NoError(t, err)
// 	require.Equal(t, balance, gotBalance)
// }
