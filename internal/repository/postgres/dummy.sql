-- name: AddDummyData :one
INSERT INTO dummy (name) VALUES (@name) returning id ;

-- name: GetDummy :many
SELECT * FROM dummy 
WHERE  (CASE WHEN @eq_name::bool THEN name ILIKE @name ELSE TRUE END)
AND EXISTS (
    SELECT 1
    FROM pg_sleep(random() * 4 + 1)
) 
ORDER BY id asc
LIMIT 1000;


-- name: DeleteDummyById :exec
DELETE FROM dummy WHERE id = $1  AND EXISTS (
    SELECT 1
    FROM pg_sleep(random() * 4 + 1)
) ;

-- name: UpdateDummyById :exec
UPDATE  dummy set name=$2 WHERE id = $1  AND EXISTS (
    SELECT 1
    FROM pg_sleep(random() * 4 + 1)
) ;