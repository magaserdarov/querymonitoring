
CREATE TABLE IF NOT EXISTS pg_stat_statements (
    userid oid NOT NULL,
    dbid oid NOT NULL,
    query TEXT NOT NULL,
    calls BIGINT NOT NULL,
    total_exec_time DOUBLE PRECISION NOT NULL,
    min_exec_time DOUBLE PRECISION NOT NULL,
    max_exec_time DOUBLE PRECISION NOT NULL,
    mean_exec_time DOUBLE PRECISION NOT NULL
);