package postgres

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/magaserdarov/querymonitoring/internal/config"
	"gitlab.com/magaserdarov/querymonitoring/internal/database"
	"go.uber.org/fx"
)

func NewPostgresClientProvider(lc fx.Lifecycle, conf *config.Config) (*sql.DB, error) {
	conn, err := database.NewPostgresConnection(conf)
	if err != nil {
		log.Print("Database connection error", err)
		return nil, err
	}

	lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			err := conn.Close()
			if err != nil {
				log.Print(err)
			}
			return err
		},
	})

	return conn, nil
}
