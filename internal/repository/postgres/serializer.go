package postgres

import (
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

func (dbQuery *GetSlowQueriesRow) Serialize(query *models.Query) {
	dbQuery.Userid = *query.Userid
	dbQuery.Dbid = *query.Dbid
	// dbQuery.Calls = sql.NullInt64(int64(calls))
	// dbQuery.MaxExecTime = query.MaxExecTime
	// dbQuery.MinExecTime = query.MinExecTime
	// dbQuery.MeanExecTime = query.MeanExecTime
	// dbQuery.TotalExecTime = query.TotalExecTime
	// dbQuery.Query = query.Query
}

func (dbQuery *GetSlowQueriesRow) Deserialize(query *models.Query) {
	query.Userid = &dbQuery.Userid
	query.Dbid = &dbQuery.Dbid
	query.Calls = dbQuery.Calls
	query.MaxExecTime = dbQuery.MaxExecTime
	query.MinExecTime = dbQuery.MinExecTime
	query.TotalExecTime = dbQuery.TotalExecTime
	query.MeanExecTime = dbQuery.MeanExecTime
	query.Query = &dbQuery.Query
}
