package postgres

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

func TestGetSlowestQueries(t *testing.T) {
	sql := "select count(*) from pg_stat_statements where query ilike $1"
	dbid := 16384
	userid := 10
	statement := "SELECT"
	testCases := []struct {
		name          string
		params        models.QueryFilters
		buildStubs    func(db *MockQueriesInterface)
		checkResponse func(err error, actual *models.QueryList)
	}{
		{name: "OK",
			params: models.QueryFilters{
				Limit:          1,
				Offset:         0,
				SortByDuration: "desc"},
			buildStubs: func(db *MockQueriesInterface) {
				args := GetSlowQueriesParams{
					GtMeanExecTime: true,
					Threshold:      0,
					OffsetRows:     0,
					PerPage:        1,
					DurationOrder:  "desc",
				}

				db.EXPECT().GetSlowQueries(gomock.Any(), args).Return([]GetSlowQueriesRow{
					{
						Calls:         4,
						Dbid:          dbid,
						MaxExecTime:   9.362917,
						MeanExecTime:  3.676177,
						MinExecTime:   1.324249,
						Query:         sql,
						TotalExecTime: 14.704708,
						Userid:        userid,
						TotalRows:     1,
					},
				}, nil).Times(1)
			}, checkResponse: func(err error, actual *models.QueryList) {
				require.NoError(t, err)
				require.Equal(t, &models.QueryList{Meta: models.Pagination{Page: 1, PerPage: 1, TotalPages: 1}, Queries: []models.Query{
					{
						Calls:         4,
						Dbid:          &dbid,
						MaxExecTime:   9.362917,
						MeanExecTime:  3.676177,
						MinExecTime:   1.324249,
						Query:         &sql,
						TotalExecTime: 14.704708,
						Userid:        &userid,
					},
				}}, actual)
			}},
		{name: "InternalServer",
			params: models.QueryFilters{
				Limit:          1,
				Offset:         2,
				SortByDuration: "desc",
				Statement:      &statement,
			},
			buildStubs: func(db *MockQueriesInterface) {
				args := GetSlowQueriesParams{
					GtMeanExecTime: true,
					Threshold:      0,
					OffsetRows:     2,
					PerPage:        1,
					DurationOrder:  "desc",
					EqStatement:    true,
					StatementName:  "%SELECT%",
				}
				db.EXPECT().GetSlowQueries(gomock.Any(), args).Return(nil, fmt.Errorf("something went wrong")).Times(1)
			}, checkResponse: func(err error, actual *models.QueryList) {
				require.Equal(t, fmt.Errorf("something went wrong"), err)
			},
		},
	}
	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			queries := NewMockQueriesInterface(ctrl)
			tc.buildStubs(queries)
			repo := NewQueriesRepo(queries)
			resp, err := repo.GetSlowQueries(context.Background(), tc.params)
			tc.checkResponse(err, resp)
		})
	}
}
