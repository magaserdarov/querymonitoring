package postgres

import (
	"context"
	"fmt"
	"math"

	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

//go:generate mockgen -source ./repository.go -package postgres -destination ./queries.mock.gen.go
type QueriesInterface interface {
	GetSlowQueries(ctx context.Context, arg GetSlowQueriesParams) ([]GetSlowQueriesRow, error)
	AddDummyData(ctx context.Context, name string) (int32, error)
	GetDummy(ctx context.Context, arg GetDummyParams) ([]Dummy, error)
	DeleteDummyById(ctx context.Context, id int32) error
	UpdateDummyById(ctx context.Context, arg UpdateDummyByIdParams) error
}

type QueriesRepo struct {
	query QueriesInterface
}

func NewQueriesRepo(queries QueriesInterface) *QueriesRepo {
	return &QueriesRepo{
		query: queries,
	}
}

func (r *QueriesRepo) GetSlowQueries(ctx context.Context, params models.QueryFilters) (*models.QueryList, error) {
	args := GetSlowQueriesParams{
		OffsetRows:    params.Offset,
		PerPage:       params.Limit,
		DurationOrder: params.SortByDuration,
	}
	if params.Statement != nil {
		args.EqStatement = true
		args.StatementName = fmt.Sprintf("%%%s%%", *params.Statement)
	}

	if params.Threshold >= 0 {
		args.GtMeanExecTime = true
		args.Threshold = params.Threshold
	}

	res, err := r.query.GetSlowQueries(ctx, args)
	if err != nil {
		return nil, err
	}

	list := &models.QueryList{}
	queries := make([]models.Query, len(res))

	for i := range res {

		res[i].Deserialize(&queries[i])
	}
	var totalRows int64
	if len(res) > 0 {
		totalRows = res[0].TotalRows
	}

	list.Queries = queries
	list.Meta = models.Pagination{
		TotalPages: int64(math.Ceil(float64(totalRows) / float64(params.Limit))),
		Page:       (params.Offset / params.Limit) + 1,
		PerPage:    params.Limit,
	}

	return list, nil

}

func (r *QueriesRepo) AddDummyData(ctx context.Context, name string) (models.Dummy, error) {
	id, err := r.query.AddDummyData(ctx, name)
	if err != nil {
		return models.Dummy{}, err
	}
	res := models.Dummy{
		Id:   id,
		Name: name,
	}
	return res, nil
}

func (s *QueriesRepo) GetDummy(ctx context.Context) ([]models.Dummy, error) {
	resp, err := s.query.GetDummy(ctx, GetDummyParams{})
	if err != nil {
		return nil, err
	}
	res := make([]models.Dummy, len(resp))
	for i := 0; i < len(resp); i++ {
		res[i] = models.Dummy{
			Id:   resp[i].ID,
			Name: resp[i].Name,
		}
	}
	return res, nil
}

func (s *QueriesRepo) DeleteDummyId(ctx context.Context, id int32) error {
	return s.query.DeleteDummyById(ctx, int32(id))
}
func (s *QueriesRepo) PutDummyId(ctx context.Context, req *models.Dummy) error {
	args := UpdateDummyByIdParams{
		ID:   req.Id,
		Name: req.Name,
	}
	err := s.query.UpdateDummyById(ctx, args)
	if err != nil {
		return err
	}
	req.Name = args.Name
	return nil
}
