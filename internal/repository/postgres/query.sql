-- name: GetSlowQueries :many
SELECT *,count(*) over () as total_rows FROM pg_stat_statements
WHERE true
  AND (CASE WHEN @gt_mean_exec_time::bool THEN mean_exec_time >=    @threshold ELSE TRUE END)
  AND (CASE WHEN @eq_statement::bool THEN query ILIKE @statement_name ELSE TRUE END)
ORDER BY
  CASE WHEN @duration_order::text = 'asc' THEN mean_exec_time END asc,
  CASE WHEN @duration_order::text = 'desc' THEN mean_exec_time END desc
LIMIT CASE WHEN @per_page::int = 0 THEN NULL ELSE @per_page END
OFFSET @offset_rows;


