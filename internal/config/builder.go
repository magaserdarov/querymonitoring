package config

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	Service ServiceConfig
	Logger  LoggerConfig
	DB      DbConfig
	App     AppConfig
}

const (
	DEFAULT_PORT                 = "8000"
	DEFAULT_QUERY_EXEC_THRESHOLD = 20
)

type LoggerConfig struct {
	Level    string
	Encoding string
}

type ServiceConfig struct {
	Name    string
	Version string
	Port    string
}

type DbConfig struct {
	DSN string
}

type AppConfig struct {
	QueryExecThreshold int
}

func New() (*Config, error) {
	replacer := strings.NewReplacer(".", "_")

	v := viper.New()
	v.SetEnvPrefix("HYGH")
	v.SetEnvKeyReplacer(replacer)
	v.AutomaticEnv()

	// set defaults
	v.SetDefault("service.port", DEFAULT_PORT)
	v.SetDefault("app.query_exec_threshold", DEFAULT_QUERY_EXEC_THRESHOLD)

	config := &Config{
		Service: ServiceConfig{
			Name:    v.GetString("service.name"),
			Version: v.GetString("service.version"),
			Port:    v.GetString("service.port"),
		},
		Logger: LoggerConfig{
			Level:    v.GetString("log.level"),
			Encoding: v.GetString("log.encoding"),
		},
		DB: DbConfig{
			DSN: v.GetString("db.dsn"),
		},
		App: AppConfig{
			QueryExecThreshold: v.GetInt("app.query_exec_threshold"),
		},
	}

	return config, nil
}
