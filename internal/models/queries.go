package models

type GetQueriesParams struct {
	Page           *int32
	PerPage        *int32
	Statement      *string
	SortByDuration *string
}

type QueryList struct {
	Meta    Pagination
	Queries []Query
}

type Query struct {
	Dbid          *int
	Userid        *int
	Calls         int64
	MaxExecTime   float64
	MeanExecTime  float64
	MinExecTime   float64
	TotalExecTime float64
	Query         *string
}

type Pagination struct {
	Page       int32
	PerPage    int32
	TotalPages int64
}

type QueryFilters struct {
	Offset         int32
	Limit          int32
	Statement      *string
	SortByDuration string
	Threshold      float64
}
