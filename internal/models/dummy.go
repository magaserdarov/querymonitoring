package models

type Dummy struct {
	Id   int32
	Name string
}

type DummyRequest struct {
	Name string
}
