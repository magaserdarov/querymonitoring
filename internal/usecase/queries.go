package usecase

import (
	"context"

	"gitlab.com/magaserdarov/querymonitoring/internal"
	"gitlab.com/magaserdarov/querymonitoring/internal/config"
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
)

type QueriesServiceImpl struct {
	conf *config.Config
	repo internal.QueryStorage
}

func NewQueriesServiceImpl(conf *config.Config, repo internal.QueryStorage) *QueriesServiceImpl {
	return &QueriesServiceImpl{repo: repo, conf: conf}
}

func (s *QueriesServiceImpl) GetQueries(ctx context.Context, params models.GetQueriesParams) (*models.QueryList, error) {

	var (
		limit          int32  = 100
		offset         int32  = 0
		sortByDuration string = "asc"
	)

	if params.PerPage != nil {
		limit = *params.PerPage
	}

	if params.Page != nil {
		offset = (*params.Page - 1) * limit
	}

	if params.SortByDuration != nil {
		sortByDuration = *params.SortByDuration
	}

	filters := models.QueryFilters{
		Statement:      params.Statement,
		Limit:          limit,
		Offset:         offset,
		SortByDuration: sortByDuration,
		Threshold:      float64(s.conf.App.QueryExecThreshold),
	}
	return s.repo.GetSlowQueries(ctx, filters)
}

func (s *QueriesServiceImpl) AddDummyData(ctx context.Context, name string) (models.Dummy, error) {
	return s.repo.AddDummyData(ctx, name)
}

func (s *QueriesServiceImpl) GetDummy(ctx context.Context) ([]models.Dummy, error) {
	return s.repo.GetDummy(ctx)
}

func (s *QueriesServiceImpl) DeleteDummyId(ctx context.Context, id int32) error {
	return s.repo.DeleteDummyId(ctx, id)
}
func (s *QueriesServiceImpl) PutDummyId(ctx context.Context, req *models.Dummy) error {
	return s.repo.PutDummyId(ctx, req)
}
