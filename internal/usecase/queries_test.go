package usecase

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/magaserdarov/querymonitoring/internal/config"
	"gitlab.com/magaserdarov/querymonitoring/internal/models"
	repoMock "gitlab.com/magaserdarov/querymonitoring/internal/repository/postgres/mocks"
)

func TestGetQueries(t *testing.T) {
	var page int32 = 1
	var perPage int32 = 1
	sort := "asc"
	//
	sql := "select count(*) from pg_stat_statements where query ilike $1"
	dbid := 16384
	userid := 10
	res := &models.QueryList{
		Queries: []models.Query{
			{
				Calls:         4,
				Dbid:          &dbid,
				MaxExecTime:   9.362917,
				MeanExecTime:  3.676177,
				MinExecTime:   1.324249,
				Query:         &sql,
				TotalExecTime: 14.704708,
				Userid:        &userid,
			},
		},
		Meta: models.Pagination{
			Page:       1,
			PerPage:    1,
			TotalPages: 1,
		},
	}
	//
	expectedList := res
	testCases := []struct {
		name          string
		params        models.GetQueriesParams
		buildStubs    func(db *repoMock.MockQueryStorage)
		checkResponse func(err error, actual *models.QueryList)
	}{
		{
			name: "OK",
			params: models.GetQueriesParams{
				Page:           &page,
				PerPage:        &perPage,
				SortByDuration: &sort,
			},
			buildStubs: func(db *repoMock.MockQueryStorage) {

				limit := perPage
				offset := (page - 1) * limit
				sortByDuration := sort

				params := models.QueryFilters{
					Limit:          limit,
					Offset:         offset,
					SortByDuration: sortByDuration,
					Threshold:      200,
				}

				db.EXPECT().GetSlowQueries(gomock.Any(), params).Return(res, nil).Times(1)
			},
			checkResponse: func(err error, actual *models.QueryList) {
				require.NoError(t, err)
				require.Equal(t, expectedList, actual)
			},
		},
		{
			name: "InternalServer",
			params: models.GetQueriesParams{
				Page:           &page,
				PerPage:        &perPage,
				SortByDuration: &sort,
			},
			buildStubs: func(db *repoMock.MockQueryStorage) {
				limit := perPage
				offset := (page - 1) * limit
				sortByDuration := sort

				params := models.QueryFilters{
					Limit:          limit,
					Offset:         offset,
					SortByDuration: sortByDuration,
					Threshold:      200,
				}
				db.EXPECT().GetSlowQueries(gomock.Any(), params).Return(nil, fmt.Errorf("something went wrong")).Times(1)
			},
			checkResponse: func(err error, actual *models.QueryList) {
				require.Equal(t, fmt.Errorf("something went wrong"), err)
			},
		},
	}
	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			db := repoMock.NewMockQueryStorage(ctrl)
			tc.buildStubs(db)
			conf := &config.Config{
				App: config.AppConfig{
					QueryExecThreshold: 200,
				},
			}
			usecase := NewQueriesServiceImpl(conf, db)
			resp, err := usecase.GetQueries(context.Background(), tc.params)
			tc.checkResponse(err, resp)

		})
	}

}
