package server

import (
	"math/rand"
	"net/http"
	"time"

	"github.com/dgraph-io/ristretto"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/magaserdarov/querymonitoring/internal/delivery/rest"
)

const (
	expiryDuration = 20 * time.Second
	jitterDuration = 10 * time.Second
)

func CacheMiddleware(store *ristretto.Cache) fiber.Handler {
	return func(c *fiber.Ctx) error {
		cacheKey := c.OriginalURL()
		path := c.Path()
		method := c.Method()

		if method != http.MethodGet || path != "/api/v1/queries" {
			println("skip cache")
			return c.Next()
		}

		if value, found := store.Get(cacheKey); found {
			if bytes, ok := value.([]byte); ok {
				c.Response().Header.Add("content-type", "application/json")
				return c.Status(http.StatusOK).Send(bytes)
			}
		}

		err := c.Next()
		expire := expiryDuration + time.Duration(rand.Intn(int(jitterDuration)))
		store.SetWithTTL(cacheKey, c.Response().Body(), 1, expire)

		return err
	}
}

func RegisterRoutes(e *fiber.App, handler rest.ServerInterface, cache *ristretto.Cache) {
	e.Get("/health", func(c *fiber.Ctx) error {
		return c.Status(http.StatusOK).JSON(struct {
			Code    int
			Message string
		}{
			Code:    200,
			Message: "ok",
		})
	})

	api := e.Group("/api")
	v1 := api.Group("/v1")
	v1.Use(CacheMiddleware(cache))

	rest.RegisterHandlers(v1, handler)
}
