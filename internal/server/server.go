package server

import (
	"context"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/magaserdarov/querymonitoring/internal/config"
	"go.uber.org/fx"
)

// New creates new echo instance
func New(lc fx.Lifecycle, shutdowner fx.Shutdowner, conf *config.Config) (*fiber.App, error) {
	engine := fiber.New(fiber.Config{
		CaseSensitive: true,
		StrictRouting: true,
		ServerHeader:  "Hygh",
	})

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go func() {
				if err := engine.Listen(fmt.Sprintf(":%s", conf.Service.Port)); err != nil {
					log.Printf("server.New fx.Hook: failed to listen port: %s - %s\n", conf.Service.Port, err)
					if err := shutdowner.Shutdown(); err != nil {
						log.Printf("server.New fx.Hook: failed to shutdown fx: %v\n", err)
					}
				}
				log.Printf("Service running on port: %s", conf.Service.Port)
			}()
			return nil
		},
		OnStop: func(ctx context.Context) error {
			// Graceful shutdown
			return engine.Shutdown()
		},
	})

	return engine, nil
}
